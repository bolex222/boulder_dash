package model;
import controller.BehaviorEndBlock;

class EndBlock extends BehaviorEndBlock{
    /**
     * constructor
     *
     * @param spritePath_
     * 			path of sprites
     * @param X_
     * 			X position
     * @param Y_
     * 			Y position
     * @throws Exception
     * 			exception
     */
    EndBlock(String spritePath_, int X_, int Y_) throws Exception {
        super(spritePath_, X_, Y_);
    }
}
